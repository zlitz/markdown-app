import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { ApiService } from './api.service';
import { MarkdownService } from './markdown.service';


@NgModule({
    imports: [
        HttpModule
    ],
    providers: [
        ApiService,
        MarkdownService
    ]
})
export class ApiModule {
}