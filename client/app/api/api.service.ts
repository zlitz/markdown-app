import { Injectable } from '@angular/core';
import { 
    Headers,
    Http,
    RequestOptionsArgs,
    Response,
    URLSearchParams
} from '@angular/http';
import { Observable } from 'rxjs/Rx';

export class ApiError {

    //--------------------------------------------------
    // Public
    //--------------------------------------------------
    public statusCode: number;
}

@Injectable()
export class ApiService {

    //--------------------------------------------------
    // Injected
    //--------------------------------------------------
    private _http: Http;

    //====================================================================================================
    // PUBLIC
    //====================================================================================================
    public constructor(http: Http) {
        this._http = http;
    }



    /**
     * 
     */
    public apiGetRequest(service: string, method: string, data?: Object): Observable<any> {

        let dataParam: string | undefined = undefined;

        if (data) {
            //Do not URI encode! The framework will handle that.
            dataParam = `request=${JSON.stringify(data)}`;
        }

        let requestOptions: RequestOptionsArgs = {
            method: 'get',
            search: dataParam ? new URLSearchParams(dataParam) : undefined,
            headers: new Headers({ "Content-Type": "application/x-www-form-urlencoded"}),
            body: dataParam
        }

        return this.makeApiRequest(service, method, requestOptions);
    }



    /**
     * 
     */
    public apiPostRequest(service: string, method: string, data?: Object): Observable<any> {

        let dataParam: string | undefined = undefined;

        if (data) {
            dataParam = `request=${encodeURIComponent(JSON.stringify(data))}`;
        }

        let requestOptions: RequestOptionsArgs = {
            method: 'post',
            headers: new Headers({ "Content-Type": "application/x-www-form-urlencoded"}),
            body: dataParam,
        }
    
        return this.makeApiRequest(service, method, requestOptions);
    }



    //====================================================================================================
    // PRIVATE
    //====================================================================================================
    /**
     * 
     */
    private handleRawApiResponse(response: Response): any {
        
        if (response.status !== 200) {
            let apiError = new ApiError();
            apiError.statusCode = response.status;
            return apiError;
        }

        return response.json();
    }



    /**
     * 
     */
    private makeApiRequest(service: string, method: string, requestOptions: RequestOptionsArgs) {

        let url = `${Environment.ServiceBase}/${service}/${method}`;
    
        return this._http.request(url, requestOptions)
        .map(response => this.handleRawApiResponse(response))
        .catch(response => {
            return Observable.throw(this.handleRawApiResponse(response))
        })
    }
}