import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Observable } from 'rxjs/Rx';

@Injectable()
export class MarkdownService {

    //--------------------------------------------------
    // Dependencies
    //--------------------------------------------------
    private _apiService: ApiService;



    //====================================================================================================
    // PUBLIC
    //====================================================================================================
    public constructor(apiService: ApiService) {
        this._apiService = apiService;
    }    

    public getMarkdown(path: string): Observable<any> {
        return this._apiService.apiGetRequest('markdown', 'get-markdown', { path: path });
    }
}