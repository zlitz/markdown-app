import { 
    AfterViewInit,
    Component
} from '@angular/core';

import { MarkdownService } from '../api/markdown.service';

declare var MathJax: any;

@Component({
    moduleId: module.id,
    selector: 'md-markdown-viewer',
    templateUrl: 'markdown-viewer.component.html',
    styleUrls: ['markdown-viewer.component.css']
})
export class MarkdownViewerComponent implements AfterViewInit {

    //====================================================================================================
    // PUBLIC
    //====================================================================================================
    public constructor(markdownService: MarkdownService) {
        markdownService.getMarkdown('some/path').subscribe(a => {
            console.log(a);
        })
    }


    public ngAfterViewInit(): void {
        console.log('Typeset')
        // MathJax.Hub.Typeset();
        MathJax.Hub.Queue(["Typeset",MathJax.Hub]);
    }

}