import {
    NgModule 
} from '@angular/core';

import { MarkdownViewerComponent } from './markdown-viewer.component';

@NgModule({
    declarations: [
        MarkdownViewerComponent
    ],
    exports: [
        MarkdownViewerComponent
    ]
})
export class MarkdownViewerModule {
    
}