import {
    Component
} from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'md-app',
    template: '<md-markdown-viewer></md-markdown-viewer>'
})
export class MainComponent {

}