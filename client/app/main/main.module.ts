import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { MainComponent } from './main.component';
import { MarkdownViewerModule } from '../markdown-viewer/markdown-viewer.module';
import { ApiModule } from '../api/api.module';

@NgModule({
    bootstrap: [
        MainComponent
    ],
    declarations: [
        MainComponent
    ],
    imports: [
        ApiModule,
        BrowserModule,
        MarkdownViewerModule,
    ]
})
export class MainModule {

}