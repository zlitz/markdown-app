import * as express from 'express';
import { normalize } from 'path';
import { ApiRouter } from './api-router';

export class Server {

    //--------------------------------------------------
    // Private
    //--------------------------------------------------
    private readonly _server: express.Application;    



    //====================================================================================================
    // PUBLIC
    //====================================================================================================
    public constructor() {
        this._server = express();
    }



    /**
     * 
     */
    public start(): void {
        this.init();

        this._server.listen(1300, () => {
            console.log(`Listening on port ${1300}!`);
        });
    }



    //====================================================================================================
    // PRIVATE
    //====================================================================================================
    /**
     * 
     */
    private init(): void {

        new ApiRouter().init(this._server);


        this._server.use('/node_modules', express.static(`${__dirname}/../node_modules`));
        this._server.use('/', express.static(`${__dirname}/../client`));
    }
}