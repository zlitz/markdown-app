import * as express from 'express';

export class ApiRouter {

    //--------------------------------------------------
    // Private
    //--------------------------------------------------
    private _router: express.Router;



    //====================================================================================================
    // PUBLIC
    //====================================================================================================
    public constructor() {

        
    }



    /**
     * 
     */
    public init(app: express.Application): void {

        this._router = express.Router();
        app.use('/api', this._router);


        this._router.get('/markdown/get-markdown', ( request, response, next) => {
            response.json({ hello: 'goodby' });
        });
    }
}